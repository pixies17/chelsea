import Foundation
import UIKit

let schemes: [[Int]] = [[1, 4, 2, 3, 1],
                        [1, 4, 3, 3],
                        [1, 3, 4, 3],
                        [1, 4, 2, 4],
                        [1, 4, 4 ,2],
                        [1, 3, 2, 4, 1]]

extension UIView {

    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
}
