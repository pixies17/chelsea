//
//  APINews.swift
//  I💙Chelsea
//
//  Created by 123 on 16.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import Foundation


enum APINews {

    static var identifier: String { "pixies" }
    static var baseURL: String {
        "https://ios-napoleonit.firebaseio.com/data/\(identifier)/"
    }
    static var storageName: String { "News" }

    static func loadNews(completion: @escaping ([News]) -> Void) {
        guard let url = URL(string: baseURL + "\(storageName).json") else { return }
        let request = URLRequest(url: url)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard
                let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? JSON
            else {
                DispatchQueue.main.async {
                    completion([])
                }
                return
            }

            var news = [News]()

            json.forEach {
                guard let value = $0.value as? JSON else { return }
                news.append(News(id: $0.key, data: value))
            }
            
            news.sort { $0.id < $1.id }

            DispatchQueue.main.async {
                completion(news)
            }
        }
        task.resume()
    }

}
