//
//  API.swift
//  I💙Chelsea
//
//  Created by 123 on 15.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import Foundation


typealias JSON = [String : Any]

enum API {

    static var identifier: String { "pixies" }
    static var baseURL: String {
        "https://ios-napoleonit.firebaseio.com/data/\(identifier)/"
    }
    static var storageName: String { "chelsea" }

    static func loadPlayers(completion: @escaping ([Player]) -> Void) {
        guard let url = URL(string: baseURL + "\(storageName).json") else { return }
        let request = URLRequest(url: url)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard
                let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? JSON
            else {
                DispatchQueue.main.async {
                    completion([])
                }
                return
            }

            var players = [Player]()

            json.forEach {
                guard let value = $0.value as? JSON else { return }
                players.append(Player(id: $0.key, data: value))
            }
            players.sort { $0.number < $1.number }


            DispatchQueue.main.async {
                completion(players)
            }
        }
        task.resume()
    }

}

