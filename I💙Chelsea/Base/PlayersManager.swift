//
//  PlayersManager.swift
//  I💙Chelsea
//
//  Created by 123 on 17/12/2019.
//  Copyright © 2019 123. All rights reserved.
//

import Foundation

class PlayersManager {
    
    static let shared = PlayersManager()
    
    func getPlayersFromUD() -> [Player] {
        let emptyPlayers = Array(repeating: Player(), count: 11)
        guard let data = UserDefaults.standard.data(forKey: "players") else { return emptyPlayers }
        let decoder = JSONDecoder()
        if let savedPlayers = try? decoder.decode(Array.self, from: data) as [Player] {
            return savedPlayers
        }
        return emptyPlayers
    }
    
    func saveToUD(_ players: [Player]) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(players)
            UserDefaults.standard.set(data, forKey: "players")
        } catch {
            print("contacts saving error")
        }
    }
    
    func getSchemeFromUD() -> [Int]? {
        guard let data = UserDefaults.standard.data(forKey: "scheme") else { return nil }
        let decoder = JSONDecoder()
        if let savedScheme = try? decoder.decode(Array.self, from: data) as [Int] {
            return savedScheme
        }
        return nil
    }
    
    func saveToUD(_ scheme: [Int]) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(scheme)
            UserDefaults.standard.set(data, forKey: "scheme")
        } catch {
            print("contacts saving error")
        }
    }
    
}
