//
//  PlayerInfoCell.swift
//  I💙Chelsea
//
//  Created by 123 on 17/12/2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class PlayerInfoCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var info: UILabel!
    
    func configureWith(titleText: String?, infoText: String?) {
        title.text = titleText
        info.text = infoText
    }

}
