//
//  NewsCell.swift
//  I💙Chelsea
//
//  Created by 123 on 16.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import SDWebImage
import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsText: UILabel!
    @IBOutlet weak var newsPhoto: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        newsText.text = nil
        newsPhoto.image = nil
    }
    
    func configureWith(post: News) {
        newsText.text = post.text
        newsText.sizeToFit()
        newsPhoto.sd_setImage(with: post.imageURL, completed: nil)
    }
    
}
