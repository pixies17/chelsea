//
//  PlayersCell.swift
//  I💙Chelsea
//
//  Created by 123 on 15.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var playerNumber: UILabel!
    @IBOutlet weak var playerPhoto: UIImageView!
    
    func configureWith(player: Player) {
        nameLabel.text = player.name + " " + player.secondaryName
        playerNumber.text = "#\(player.number)"
        playerPhoto.image = UIImage(named: "\(player.number)")
    }
    
}
