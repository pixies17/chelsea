//
//  NewsfeedVC.swift
//  I💙Chelsea
//
//  Created by 123 on 16.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var news: [News] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        tableView.alwaysBounceVertical = false
        tableView.tableFooterView = .init(frame: .zero)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Spinner.shared.show(in: view)
        APINews.loadNews { response in
            Spinner.shared.hide()
            self.news = response
        }
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

}
extension NewsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell") as? NewsCell else {
            return UITableViewCell()
        }
        
        cell.configureWith(post: news[indexPath.row])
        return cell
    }

}

