//
//  PlayerInfoVC.swift
//  I💙Chelsea
//
//  Created by 123 on 17/12/2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class PlayerInfoVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var photoView: UIImageView!
    
    var player: Player?
    
    var dataSource: [(key: String, value: String?)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    func configureUI() {
        guard let player = player else {return}
        
        photoView.image = UIImage(named: "\(player.number)")
        dataSource = [("Имя", player.name + " " + player.secondaryName),
                      ("Возраст", player.age),
                      ("Номер", "#\(player.number)"),
                      ("День рождения", player.dateOfBirth),
                      ("Страна", player.birthPlace),
                      ("Позиция", player.position),
                      ("Рост", (player.height ?? "") + " м"),
                      ("Вес", (player.weight ?? "") + " кг")]
        
        tableView.tableFooterView = .init(frame: .zero)
        tableView.reloadData()
    }

}

extension PlayerInfoVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "playerInfoCell") as? PlayerInfoCell else {
            return UITableViewCell()
        }
        
        cell.configureWith(titleText: dataSource[indexPath.row].key, infoText: dataSource[indexPath.row].value)
        return cell
    }
    
    
}
