//
//  LineUpVCViewController.swift
//  I💙Chelsea
//
//  Created by 123 on 15/12/2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class LineUpVC: UIViewController {

    @IBOutlet weak var linesStackView: UIStackView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var playerPickerView: UIPickerView!
    
    enum LineUpState {
        case schemeEdit
        case playersEdit
        case usuall
    }
    
    var editingCard: PlayerView?
    var playersOnScheme: [Player] = PlayersManager.shared.getPlayersFromUD() {
        didSet {
            PlayersManager.shared.saveToUD(playersOnScheme)
        }
    }
    
    var state: LineUpState = .usuall {
        didSet {
            switch state {
            case .usuall:
                editingCard = nil
                navigationItem.rightBarButtonItem?.title = "Схемы"
            case .schemeEdit:
                editingCard = nil
                navigationItem.rightBarButtonItem?.title = "Сохранить"
            case .playersEdit:
                playerPickerView.selectRow(0, inComponent: 0, animated: true)
                navigationItem.rightBarButtonItem?.title = "Сохранить"
            }
            UIView.animate(withDuration: 0.3) {
                self.pickerView.alpha = self.state == .schemeEdit ? 1 : 0
                self.playerPickerView.alpha = self.state == .playersEdit ? 1 : 0
            }
        }
    }
    
    var scheme: [Int] = PlayersManager.shared.getSchemeFromUD() ?? [1, 3, 2, 4, 1] {
        didSet {
            PlayersManager.shared.saveToUD(scheme)
            UIView.animate(withDuration: 0.2, animations: {
                self.linesStackView.alpha = 0
            }) { _ in
                self.linesStackView.subviews.forEach { $0.removeFromSuperview() }
                self.configureStackViews()
                UIView.animate(withDuration: 0.2) {
                    self.linesStackView.alpha = 1
                }
            }
        }
    }
    
    var cachedPlayers: [Player] = [] {
        didSet {
            playerPickerView.delegate = self
            playerPickerView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureStackViews()
        configurePicker()
        updateNavTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Spinner.shared.show(in: view)
        API.loadPlayers { response in
            Spinner.shared.hide()
            var mResponse = response
            mResponse.insert(Player(), at: 0)
            self.cachedPlayers = mResponse
        }
    }
    
    private func configureStackViews() {
        var countOfAddedPlayers = 0
        
        for lineCount in scheme {
            let horizontalStackView = UIStackView()
            horizontalStackView.alignment = .fill
            horizontalStackView.axis = .horizontal
            horizontalStackView.distribution = .fillEqually
            
            for _ in 0..<lineCount {
                guard let playerView = Bundle.main.loadNibNamed("PlayerView", owner: self, options: nil)?.first as? PlayerView else {continue}
                playerView.delegate = self
                playerView.configureWith(playersOnScheme[countOfAddedPlayers])
                horizontalStackView.addArrangedSubview(playerView)
                countOfAddedPlayers += 1
            }
            
            linesStackView.addArrangedSubview(horizontalStackView)
        }
    }
    
    private func configurePicker() {
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    private func updateNavTitle() {
        navigationItem.title = getSchemeDescription(scheme.reversed())
    }
    
    private func getSchemeDescription(_ lScheme: [Int]) -> String {
        var desc = lScheme.description
        desc.removeFirst()
        desc.removeFirst()
        desc.removeFirst()
        desc.removeLast()
        desc = desc.replacingOccurrences(of: ", ", with: "-")
        return desc
    }
    
    private func updatePlayerInDataSource(_ player: Player) {
        var countOfCheckedViews = 0
        for horizontalStackView in linesStackView.subviews {
            for view in horizontalStackView.subviews {
                guard let playerView = view as? PlayerView else { continue }
                
                if playerView == editingCard {
                    playersOnScheme[countOfCheckedViews] = player
                    break
                }
                countOfCheckedViews += 1
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func editButtonTapped(_ sender: Any) {
        state = state == .usuall ? .schemeEdit : .usuall
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let screenshot = view.asImage()
        
        let activityViewController = UIActivityViewController(activityItems: ["Мой состав", screenshot], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
    }
    
}

extension LineUpVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == playerPickerView {
            return cachedPlayers.count
        }
        
        return schemes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == playerPickerView {
            return cachedPlayers[row].name + " " + cachedPlayers[row].secondaryName
        }
        
        return getSchemeDescription(schemes[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == playerPickerView {
            updatePlayerInDataSource(cachedPlayers[row])
            editingCard?.configureWith(cachedPlayers[row])
            return
        }
        
        scheme = schemes[row].reversed()
        updateNavTitle()
    }
    
}

extension LineUpVC: PlayerViewDelegate {
    func playerCardTapped(sender: PlayerView) {
        state = .playersEdit
        editingCard = sender
    }
}
