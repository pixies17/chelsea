import UIKit

protocol PlayerViewDelegate: class {
    func playerCardTapped(sender: PlayerView)
}

class PlayerView: UIView {

    @IBOutlet weak var playerPhotoView: UIImageView!
    @IBOutlet weak var playerNameLabel: UILabel!
    
    weak var delegate: PlayerViewDelegate?
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureWith(_ player: Player) {
        playerPhotoView.contentMode = player.isEmpty() ? .center : .scaleAspectFit
        playerPhotoView.image = UIImage(named: "\(player.number)")
        playerNameLabel.text = player.secondaryName.isEmpty ? player.name : player.secondaryName
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
        addGestureRecognizer(gesture)
    }
    
    @objc func tapped() {
        delegate?.playerCardTapped(sender: self)
    }
    
    static func loadFromNib() -> PlayerView {
        return Bundle.main.loadNibNamed("PlayerView", owner: self, options: nil)?.first as! PlayerView
    }
    
}
