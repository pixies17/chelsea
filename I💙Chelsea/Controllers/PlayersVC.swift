//
//  Player.swift
//  I💙Chelsea
//
//  Created by 123 on 15.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import UIKit

class PlayersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var players: [Player] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Spinner.shared.show(in: view)
        API.loadPlayers { response in
            Spinner.shared.hide()
            self.players = response
        }
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = .init(frame: .zero)
    }
}

extension PlayersVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell") as? PlayerCell else {
            return UITableViewCell()
        }
        
        cell.configureWith(player: players[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let player = players[indexPath.row]
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "playersInfoVC") as! PlayerInfoVC
        vc.player = player
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
