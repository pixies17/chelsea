//
//  News.swift
//  I💙Chelsea
//
//  Created by 123 on 16.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import Foundation

struct News: Codable, Equatable {
    let id: String
    let text: String
    let imageURL: URL?
}
    
extension News {
    init(id: String, data: JSON) {
        self.id = id
        self.text  = data["text"] as? String ?? ""
        self.imageURL = URL(string: data["url"] as? String ?? "")
    }
}
