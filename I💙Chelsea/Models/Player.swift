//
//  Player.swift
//  I💙Chelsea
//
//  Created by 123 on 15.12.2019.
//  Copyright © 2019 123. All rights reserved.
//

import Foundation

struct Player: Codable, Equatable {
    
    let emptyId = -1
    
    let id: String
    let name: String
    let secondaryName: String
    let number: Int
    let age: String?
    let dateOfBirth: String?
    let birthPlace: String?
    let height: String?
    let weight: String?
    let position: String?
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.name == rhs.name && lhs.secondaryName == rhs.secondaryName
    }
    
}

extension Player {
    init(id: String, data: JSON) {
        self.id = id
        self.name = data["name"] as? String ?? ""
        self.secondaryName = data["secondaryName"] as? String ?? ""
        self.number = Int(data["number"] as? String ?? "0") ?? 0
        self.age = data["age"] as? String
        self.dateOfBirth = data["dateOfBirth"] as? String
        self.birthPlace = data["birthPlace"] as? String
        self.height = data["height"] as? String
        self.weight = data["weight"] as? String
        self.position = data["position"] as? String
    }
    
    // empty player
    init() {
        self.id = "-1"
        self.name = ""
        self.secondaryName = "Добавить"
        self.number = emptyId
        self.age = nil
        self.dateOfBirth = nil
        self.birthPlace = nil
        self.height = nil
        self.weight = nil
        self.position = nil
    }
    
    func isEmpty() -> Bool {
        return number == emptyId
    }
}
